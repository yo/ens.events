export const GridLayout = ({ children }) => {
  return (
    <div className="container mx-auto max-w-screen-2xl flex-grow mb-10 py-8 lg:px-52 md:px-10 sm:px-5 px-5 pb-16 lg:pb-0">
      <div className="grid grid-cols-12 lg:gap-8">{children}</div>
    </div>
  );
};

export const GridItemFour = ({ children }) => {
  return (
    <div className="lg:col-span-4 md:col-span-12 col-span-12">{children}</div>
  );
};

export const GridItemEight = ({ children, className = "" }) => {
  return (
    <div className="lg:col-span-8 md:col-span-12 col-span-12 mb-5 space-y-3">
      {children}
    </div>
  );
};
