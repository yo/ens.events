/* eslint-disable @next/next/no-img-element */
import { ExclamationIcon } from "@heroicons/react/solid";
import CopyToClipboard from "react-copy-to-clipboard";
import toast from "react-hot-toast";
import { useAvatar } from "../hooks/useAvatar";
import { GradientText } from "../UI/GradientText";
import { formatID } from "../utils/formatID";

export const Profile = ({ data, textRecords }) => {
  const { avatar } = useAvatar({ ens: data.domain.name });
  const expired = (
    (new Date() - new Date(data.expiryDate * 1000)) /
    (1000 * 60 * 60 * 24)
  ).toFixed();

  return (
    <div className="space-y-5">
      <div className="bg-gray-200 rounded-2xl">
        {!avatar ? (
          <img
            src="/default.jpeg"
            className="rounded-2xl w-full"
            alt="Default Avatar"
          />
        ) : (
          <img src={avatar} className="rounded-2xl w-full" alt={avatar} />
        )}
        <div className="px-5 py-3 space-y-1">
          <div className="font-bold text-2xl truncate">{data.domain.name}</div>
          <CopyToClipboard
            text={data.domain.id}
            onCopy={() => {
              toast.success("Address copied!");
            }}
          >
            <div className="text-xs flex items-center space-x-1">
              <div>ID:</div>
              <GradientText>{formatID(data.domain.owner.id)}</GradientText>
            </div>
          </CopyToClipboard>
        </div>
      </div>
      {expired >= 0 && expired <= 90 && (
        <div className="text-yellow-600 flex items-center space-x-1 font-bold">
          <ExclamationIcon className="h-5 w-5" />
          <div>
            Grace period ends{" "}
            {90 - expired === 0 ? "today" : `in ${90 - expired} days`}
          </div>
        </div>
      )}
      {expired > 90 && (
        <div className="text-red-600 flex items-center space-x-1 font-bold">
          <ExclamationIcon className="h-5 w-5" />
          <div>Expired</div>
        </div>
      )}
      <div>{textRecords?.data?.description}</div>
      {(textRecords?.data?.["com.twitter"] ||
        textRecords?.data?.["com.github"] ||
        textRecords?.data?.["com.linkedin"] ||
        textRecords?.data?.["org.telegram"]) && (
        <div>
          <div className="font-bold text-lg">Social</div>
          <div className="space-y-1 mt-2">
            {textRecords?.data?.["com.twitter"] && (
              <a
                href={`https://twitter.com/${textRecords?.data["com.twitter"]}`}
                className="flex items-center space-x-1"
                target="_blank"
                rel="noreferrer"
              >
                <img className="h-4 w-4" src="/twitter.svg" alt="Twitter" />
                <div>{textRecords?.data["com.twitter"]}</div>
              </a>
            )}
            {textRecords?.data?.["com.github"] && (
              <a
                href={`https://github.com/${textRecords?.data["com.github"]}`}
                className="flex items-center space-x-1"
                target="_blank"
                rel="noreferrer"
              >
                <img className="h-4 w-4" src="/github.svg" alt="GitHub" />
                <div>{textRecords?.data["com.github"]}</div>
              </a>
            )}
            {textRecords?.data?.["com.linkedin"] && (
              <a
                href={`https://linkedin.com/in/${textRecords?.data["com.linkedin"]}`}
                className="flex items-center space-x-1"
                target="_blank"
                rel="noreferrer"
              >
                <img className="h-4 w-4" src="/linkedin.svg" alt="LinkedIn" />
                <div>{textRecords?.data["com.linkedin"]}</div>
              </a>
            )}
            {textRecords?.data?.["org.telegram"] && (
              <a
                href={`https://t.me/${textRecords?.data["org.telegram"]}`}
                className="flex items-center space-x-1"
                target="_blank"
                rel="noreferrer"
              >
                <img className="h-4 w-4" src="/telegram.svg" alt="Telegram" />
                <div>{textRecords?.data["org.telegram"]}</div>
              </a>
            )}
          </div>
        </div>
      )}
    </div>
  );
};
