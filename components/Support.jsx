/* eslint-disable @next/next/no-img-element */

import { HomeIcon } from "@heroicons/react/outline";
import Link from "next/link";

export default function Support() {
  return (
    <div className="fixed bottom-0 bg-gray-200 border-t border-gray-300 w-full flex justify-between text-gray-600 text-xs px-5 py-2">
      <div className="flex items-center space-x-2">
        <Link href="/" passHref>
          <HomeIcon className="h-4 w-4 cursor-pointer" />
        </Link>
        <div className="text-gray-500">
          Support at{" "}
          <span className="font-bold">
            0x3A5bd1E37b099aE3386D13947b6a90d97675e5e3
          </span>
        </div>
      </div>
      <a
        href="https://etherscan.io/address/yoginth.eth"
        className="hidden sm:flex items-center space-x-1.5"
        target="_blank"
        rel="noreferrer"
      >
        <span>made with</span>
        <img src="/heart.png" alt="Heart" className="h-3" />
        <b>by yoginth.eth</b>
      </a>
    </div>
  );
}
