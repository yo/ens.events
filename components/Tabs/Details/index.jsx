import { ExternalLinkIcon, HashtagIcon } from "@heroicons/react/solid";
import { KeyValue } from "../../UI/KeyValue";
import { formatID } from "../../utils/formatID";
import { Address } from "../../UI/Address";

const _ = require("lodash");

export const Details = ({ data }) => {
  const sortedEvents = _.orderBy(
    data.events,
    [
      function (o) {
        return o.blockNumber;
      },
    ],
    ["desc"]
  );

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-2xl divide-y divide-gray-300">
      <KeyValue title="Registration ID" value={data.id} />
      <KeyValue title="Domain ID" value={data.domain.id} />
      <KeyValue
        title="Parent"
        value={data.domain.parent.name}
        showCopy={false}
      />
      <KeyValue
        title="Registrant"
        value={<Address address={data.registrant.id} />}
      />
      <KeyValue
        title="Controller"
        value={<Address address={data.domain.owner.id} />}
      />
      {data.domain.resolver?.address && (
        <KeyValue
          title="Resolver"
          value={<Address address={data.domain.resolver.address} />}
        />
      )}
      <KeyValue
        title="Registration Date"
        value={new Date(data.registrationDate * 1000).toString()}
        showCopy={false}
      />
      <KeyValue
        title="Expiry Date"
        value={new Date(data.expiryDate * 1000).toString()}
        showCopy={false}
      />
      <KeyValue
        title="Registration Events"
        value={
          <div className="border border-gray-300 rounded-lg divide-y divide-gray-300 w-full mt-2">
            {sortedEvents.map((event, index) => (
              <div
                key={index}
                className="p-1.5 flex items-center space-x-10 justify-between"
              >
                <div className="text-sm">
                  <b>{event.__typename}</b> on block{" "}
                  <a
                    className="text-blue-500"
                    href={`https://etherscan.io/block/${event.blockNumber}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    <b>{event.blockNumber}</b>
                  </a>
                </div>
                <a
                  className="text-blue-500 font-bold text-sm flex items-center space-x-1"
                  href={`https://etherscan.io/tx/${event.transactionID}`}
                  target="_blank"
                  title="Transaction"
                  rel="noreferrer"
                >
                  <div>{formatID(event.transactionID)}</div>
                  <ExternalLinkIcon className="h-4 w-4" />
                </a>
              </div>
            ))}
          </div>
        }
        showCopy={false}
      />
    </div>
  );
};
