import { utils } from "ethers";

export const NameRenewed = ({ log }) => {
  return (
    <>
      {log.length === 4 && (
        <div className="space-y-1.5">
          <div>
            Name:{" "}
            <span className="bg-gray-300 px-2 py-0.5 rounded-lg border border-gray-400 font-bold text-xs">
              {log[0]}
            </span>
          </div>
          <div>
            Cost:{" "}
            <span className="bg-gray-300 px-2 py-0.5 rounded-lg border border-gray-400 font-bold text-xs">
              {utils.formatEther(log[2]).substring(0, 6)} Ξ
            </span>
          </div>
          <div>
            Expires:{" "}
            <span className="bg-gray-300 px-2 py-0.5 rounded-lg border border-gray-400 font-bold text-xs">
              {new Date(log[3] * 1000).toString()}
            </span>
          </div>
        </div>
      )}
    </>
  );
};
