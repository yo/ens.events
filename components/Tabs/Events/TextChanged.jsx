import { formatID } from "../../utils/formatID";

export const TextChanged = ({ isIndexed, log }) => {
  return (
    <>
      {log && (
        <div className="block sm:flex space-x-1">
          <div>{isIndexed ? "Text changed: " : "Address changed: "}</div>
          <div className="bg-gray-300 px-2 py-0.5 rounded-lg border border-gray-400 font-bold text-xs w-fit">
            {isIndexed ? log : formatID(log, 10)}
          </div>
        </div>
      )}
    </>
  );
};
