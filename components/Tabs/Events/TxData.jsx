import { BigNumber, providers, utils } from "ethers";
import { useEffect, useState } from "react";
import { RPC_URL } from "../../utils/constants";
import { formatID } from "../../utils/formatID";
import { Address } from "../../UI/Address";
import { ArrowSmRightIcon } from "@heroicons/react/outline";
import * as dayjs from "dayjs";
import * as relativeTime from "dayjs/plugin/relativeTime";
import ensABI from "../../abis/ens.json";
import { TextChanged } from "./TextChanged";
import { NameRenewed } from "./NameRenewed";

dayjs.extend(relativeTime);

export const TxData = ({ txID, type }) => {
  const [txData, setTxData] = useState([]);
  const [blockData, setBlockData] = useState([]);
  const [loading, setLoading] = useState(false);

  const provider = new providers.JsonRpcProvider(RPC_URL);
  const iface = new utils.Interface(ensABI);

  const getTransaction = async () => {
    const transaction = await provider.getTransactionReceipt(txID);
    const block = await provider.getBlock(transaction.blockNumber);
    setTxData(transaction);
    setBlockData(block);
  };

  const getLogData = (log) => {
    if (type === "TextChanged" || type === "NameRenewed") {
      const decodedInput = iface.parseLog(log);

      return decodedInput.args;
    } else {
      return false;
    }
  };

  useEffect(() => {
    setLoading(true);
    getTransaction().finally(() => setLoading(false));
  }, []);

  const gasUsed = BigNumber.from(txData?.gasUsed || 0);
  const effectiveGasPrice = BigNumber.from(txData?.effectiveGasPrice || 0);
  const gasTotal = gasUsed.mul(effectiveGasPrice);

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-xl divide-y divide-gray-300">
      {loading && txData ? (
        <div className="p-3">
          <div className="text-sm font-bold">Loading transaction...</div>
        </div>
      ) : (
        <div className="p-3 text-sm">
          <div>
            Block Hash: <b>{formatID(txData.blockHash, 8)}</b>
          </div>
          <div>
            Confirmations: <b>{txData.confirmations}</b>
          </div>
          <div title={utils.formatEther(gasTotal) + " ETH"}>
            Gas used: <b>{utils.formatEther(gasTotal).substring(0, 6)} Ξ</b>
          </div>
          <div title={new Date(blockData.timestamp * 1000).toString()}>
            Happened:{" "}
            <b>{dayjs(new Date(blockData.timestamp * 1000)).fromNow()}</b>
          </div>
          <div className="flex items-center space-x-1.5 mt-1">
            <Address address={txData.from} avatarSize={15} shorten />
            <ArrowSmRightIcon className="h-5 w-5 text-green-500" />
            <Address address={txData.to} avatarSize={15} shorten />
          </div>
          {txData.logs?.length !== 0 &&
            (type === "TextChanged" || type === "NameRenewed") && (
              <div>
                <div className="font-bold mt-2 mb-1">Logs</div>
                <div className="space-y-1.5">
                  {txData.logs?.map((log, index) => (
                    <div key={index}>
                      {type === "TextChanged" && (
                        <TextChanged
                          isIndexed={getLogData(log)[1]._isIndexed}
                          log={getLogData(log)[2]}
                        />
                      )}
                      {type === "NameRenewed" && (
                        <NameRenewed log={getLogData(log)} />
                      )}
                    </div>
                  ))}
                </div>
              </div>
            )}
        </div>
      )}
    </div>
  );
};
