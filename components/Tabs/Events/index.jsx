import { ExternalLinkIcon } from "@heroicons/react/outline";
import { formatID } from "../../utils/formatID";
import { TxData } from "./TxData";
const _ = require("lodash");

export const Events = ({ data }) => {
  const resolverEvents = data.domain.resolver
    ? data.domain.resolver.events.map((o) => ({
        ...o,
        persona: "Resolver",
      }))
    : [];

  const controllerEvents = data.domain
    ? data.domain.events.map((o) => ({
        ...o,
        persona: "Controller",
      }))
    : [];

  const allEvents = [
    ...data.events.map((o) => ({
      ...o,
      persona: "Registrar",
    })),
    ...controllerEvents,
    ...resolverEvents,
  ];
  const uniqueEvents = _.uniqBy(allEvents, function (o) {
    return o.transactionID;
  });
  const sortedEvents = _.orderBy(
    uniqueEvents,
    [
      function (o) {
        return o.blockNumber;
      },
    ],
    ["desc"]
  );

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-2xl divide-y divide-gray-300">
      {sortedEvents.map((event, index) => (
        <div key={index} className="p-5 space-y-3">
          <div className="flex items-center justify-between">
            <div>
              <div>
                {event.__typename}{" "}
                <span className="text-gray-800 text-xs">({event.persona})</span>
              </div>
              <div className="text-sm font-bold">
                on block{" "}
                <a
                  className="text-blue-500"
                  href={`https://etherscan.io/block/${event.blockNumber}`}
                  target="_blank"
                  rel="noreferrer"
                >
                  {event.blockNumber}
                </a>
              </div>
            </div>
            <a
              className="text-blue-500 font-bold text-sm flex items-center space-x-1"
              href={`https://etherscan.io/tx/${event.transactionID}`}
              target="_blank"
              title="Transaction"
              rel="noreferrer"
            >
              <div>{formatID(event.transactionID, 7)}</div>
              <ExternalLinkIcon className="h-4 w-4" />
            </a>
          </div>
          <TxData txID={event.transactionID} type={event.__typename} />
        </div>
      ))}
    </div>
  );
};
