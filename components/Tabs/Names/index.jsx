import { Address } from "../../UI/Address";

export const Names = ({ data }) => {
  const allNames = data.registrant.domains;

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-2xl divide-y divide-gray-300">
      {allNames.map(
        (domain, index) =>
          !domain.name.startsWith("[") && (
            <div key={`domain-${index}`} className="p-5 space-y-2">
              <div>
                <a href={`/${domain.name}`} className="font-bold">
                  <div className="truncate">{domain.name}</div>
                </a>
                <div className="text-sm flex items-center space-x-1">
                  <div>resolving to</div>
                  {domain?.resolvedAddress?.id ? (
                    <div className="font-bold">
                      <Address
                        address={domain.resolvedAddress.id}
                        avatarSize={15}
                        shorten
                      />
                    </div>
                  ) : (
                    <i>nothing</i>
                  )}
                </div>
              </div>
              <div className="ml-10 space-y-2">
                {domain.subdomains.map(
                  (subdomain, index) =>
                    !subdomain.name.startsWith("[") && (
                      <div key={`subdomain-${index}`}>
                        <a href={`/${subdomain.name}`} className="font-bold">
                          <div className="truncate">↳ {subdomain.name}</div>
                        </a>
                        <div className="text-sm">
                          resolving to{" "}
                          {subdomain?.resolvedAddress?.id ? (
                            <div className="font-bold">
                              <Address
                                address={subdomain.resolvedAddress.id}
                                avatarSize={15}
                                shorten
                              />
                            </div>
                          ) : (
                            <i>nothing</i>
                          )}
                        </div>
                      </div>
                    )
                )}
              </div>
            </div>
          )
      )}
    </div>
  );
};
