import { CollectionIcon } from "@heroicons/react/outline";
import { providers } from "ethers";
import { useEffect, useState } from "react";
import { KeyValue } from "../../UI/KeyValue";
import { Spinner } from "../../UI/Spinner";
import { RPC_URL } from "../../utils/constants";

export const Records = ({ data }) => {
  const [records, setRecords] = useState([]);
  const [loading, setLoading] = useState(false);

  const populateValues = async () => {
    const provider = new providers.JsonRpcProvider(RPC_URL);
    const resolver = await provider.getResolver(data?.domain?.name);
    const texts = data.domain?.resolver?.texts;
    if (texts?.length > 0) {
      const textRecords = texts.map((key) =>
        resolver?.getText(key).then((value) => ({ key, value }))
      );
      setRecords(await Promise.all(textRecords));
    }
  };

  useEffect(() => {
    setLoading(true);
    populateValues().finally(() => setLoading(false));
  }, []);

  const getValue = (key, value) => {
    if (key === "email") {
      return (
        <a className="link" href={`mailto://${value}`}>
          {value}
        </a>
      );
    } else if (key === "url") {
      return (
        <a className="link" href={value} target="_blank" rel="noreferrer">
          {value}
        </a>
      );
    } else if (key === "com.twitter") {
      return (
        <a
          className="link"
          href={`https://twitter.com/${value}`}
          target="_blank"
          rel="noreferrer"
        >
          {value}
        </a>
      );
    } else if (key === "com.github") {
      return (
        <a
          className="link"
          href={`https://github.com/${value}`}
          target="_blank"
          rel="noreferrer"
        >
          {value}
        </a>
      );
    } else if (key === "com.linkedin") {
      return (
        <a
          className="link"
          href={`https://linkedin.com/in/${value}`}
          target="_blank"
          rel="noreferrer"
        >
          {value}
        </a>
      );
    } else if (key === "org.telegram") {
      return (
        <a
          className="link"
          href={`https://t.me/${value}`}
          target="_blank"
          rel="noreferrer"
        >
          {value}
        </a>
      );
    } else if (key === "eth.ens.delegate") {
      return (
        <a className="link" href={value} target="_blank" rel="noreferrer">
          {value}
        </a>
      );
    } else {
      return value;
    }
  };

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-2xl divide-y divide-gray-300">
      {loading ? (
        <div className="p-5 text-center space-y-2">
          <Spinner className="mx-auto" />
          <div className="font-bold">Loading text records</div>
        </div>
      ) : records.length === 0 ? (
        <div className="p-5 text-center space-y-2">
          <CollectionIcon className="mx-auto h-8 w-8 text-blue-500" />
          <div className="font-bold">No text records found!</div>
        </div>
      ) : (
        records.map(
          (record, index) =>
            record.value?.length > 0 && (
              <KeyValue
                key={index}
                title={record.key}
                value={getValue(record.key, record.value)}
              />
            )
        )
      )}
    </div>
  );
};
