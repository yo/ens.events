import { CollectionIcon } from "@heroicons/react/outline";
import { KeyValue } from "../../UI/KeyValue";

export const Wallets = ({ data }) => {
  const wallets = data?.data?.wallets;

  return (
    <div className="bg-gray-200 border border-gray-300 rounded-2xl divide-y divide-gray-300">
      {wallets?.length === 0 || !data.success ? (
        <div className="p-5 text-center space-y-2">
          <CollectionIcon className="mx-auto h-8 w-8 text-blue-500" />
          <div className="font-bold">No wallets found!</div>
        </div>
      ) : (
        wallets?.map(
          (wallet) =>
            wallet.value && (
              <KeyValue
                key={wallet.key}
                title={wallet.name}
                value={wallet.value}
              />
            )
        )
      )}
    </div>
  );
};
