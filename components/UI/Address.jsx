import { formatID } from "../utils/formatID";
import Jazzicon, { jsNumberForAddress } from "react-jazzicon";

export const Address = ({ address, avatarSize = 20, shorten = false }) => {
  return (
    <a
      className="flex items-center space-x-1.5"
      href={`https://etherscan.io/address/${address}`}
      target="_blank"
      rel="noreferrer"
    >
      <Jazzicon
        diameter={avatarSize}
        seed={jsNumberForAddress(address || "")}
      />
      <div>{shorten ? formatID(address) : address}</div>
    </a>
  );
};
