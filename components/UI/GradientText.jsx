export const GradientText = ({ children }) => {
  return (
    <div className="cursor-pointer font-extrabold text-transparent bg-clip-text bg-gradient-to-br from-pink-400 to-red-600">
      {children}
    </div>
  );
};
