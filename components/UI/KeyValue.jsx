import CopyToClipboard from "react-copy-to-clipboard";
import toast from "react-hot-toast";
import { DuplicateIcon } from "@heroicons/react/outline";

export const KeyValue = ({ title, value, showCopy = true }) => {
  return (
    <div className="space-y-1 px-5 py-4">
      <div className="font-bold text-gray-700">{title}</div>
      <CopyToClipboard
        text={showCopy ? value : null}
        onCopy={() => {
          showCopy && toast.success(`${title} copied!`);
        }}
      >
        <div className="flex items-center space-x-1">
          <div className="truncate">{value}</div>
          {showCopy && (
            <DuplicateIcon className="h-4 w-4 text-gray-500 cursor-pointer" />
          )}
        </div>
      </CopyToClipboard>
    </div>
  );
};
