export const Spinner = ({ className }) => {
  return (
    <div
      className={`h-8 w-8 border-[3px] rounded-full animate-spin border-blue-200 border-t-blue-500 ${className}`}
    />
  );
};
