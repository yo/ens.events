import { providers } from "ethers";
import { useEffect, useState } from "react";
import { RPC_URL } from "../utils/constants";

export const useAvatar = ({ ens }) => {
  const [avatar, setAvatar] = useState();

  const populateENS = async () => {
    try {
      if (ens) {
        const provider = new providers.JsonRpcProvider(RPC_URL);
        const resolver = await provider.getResolver(ens);
        const avatar = await resolver?.getAvatar();
        setAvatar(avatar?.url);
        return;
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    populateENS();
  }, [ens]);

  return { avatar };
};
