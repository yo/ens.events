export const formatID = (id, length = 4) => {
  if (!id) return "";

  return `${id.slice(0, 4)}…${id.slice(id.length - length, id.length)}`;
};
