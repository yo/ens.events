import { createClient, gql } from "urql";
import namehash from "eth-ens-namehash";
import {
  GridItemEight,
  GridItemFour,
  GridLayout,
} from "../components/GridLayout";
import { Profile } from "../components/Profile";
import { Details } from "../components/Tabs/Details";
import { useState } from "react";
import { Records } from "../components/Tabs/Records";
import { Events } from "../components/Tabs/Events";
import Head from "next/head";
import { Names } from "../components/Tabs/Names";
import { Wallets } from "../components/Tabs/Wallets";
import Support from "../components/Support";
import {
  CollectionIcon,
  CreditCardIcon,
  CubeIcon,
  FolderOpenIcon,
  IdentificationIcon,
  InformationCircleIcon,
} from "@heroicons/react/outline";
import { API_URL } from "../components/utils/constants";

const ensQuery = gql`
  query ($id: String) {
    registration(id: $id) {
      id
      registrationDate
      expiryDate
      registrant {
        id
        domains {
          name
          resolvedAddress {
            id
          }
          subdomains {
            name
            resolver {
              address
            }
          }
        }
      }
      events {
        transactionID
        blockNumber
      }
      domain {
        id
        name
        resolvedAddress {
          id
        }
        owner {
          id
        }
        parent {
          name
        }
        events {
          transactionID
          blockNumber
        }
        resolver {
          address
          texts
          coinTypes
          events {
            transactionID
            blockNumber
          }
        }
      }
    }
  }
`;

const client = createClient({ url: API_URL });

const TabButton = ({ children, icon, active = false, onClick }) => (
  <button
    className={`rounded-lg border border-blue-300 hover:border-blue-400 bg-blue-200 hover:bg-blue-300 px-5 py-1 text-sm font-bold ${
      active && "bg-blue-400 border-blue-500"
    }`}
    onClick={onClick}
  >
    <div className="flex items-center space-x-1.5">
      <div>{icon}</div>
      <div>{children}</div>
    </div>
  </button>
);

export default function Dashboard({ registrationData, textRecords }) {
  const [tab, setTab] = useState("HOME");
  const title = `${registrationData.domain.name} | ENS Debugger`;
  const description = `ENS debugger result for ${registrationData.domain.name}`;

  return (
    <>
      <GridLayout>
        <Head>
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta property="og:title" content={title} />
          <meta property="og:description" content={description} />
          <meta property="og:image" content={textRecords?.data?.avatar} />
          <meta property="twitter:title" content={title} />
          <meta property="twitter:description" content={description} />
          <meta property="twitter:image" content={textRecords?.data?.avatar} />
        </Head>
        <GridItemFour>
          <Profile data={registrationData} textRecords={textRecords} />
        </GridItemFour>
        <GridItemEight>
          <div className="space-x-3">
            <TabButton
              onClick={() => setTab("HOME")}
              active={tab === "HOME"}
              icon={<IdentificationIcon className="h-4 w-4" />}
            >
              Details
            </TabButton>
            <TabButton
              onClick={() => setTab("RECORDS")}
              active={tab === "RECORDS"}
              icon={<FolderOpenIcon className="h-4 w-4" />}
            >
              Records
            </TabButton>
            <TabButton
              onClick={() => setTab("WALLETS")}
              active={tab === "WALLETS"}
              icon={<CreditCardIcon className="h-4 w-4" />}
            >
              Wallets
            </TabButton>
            <TabButton
              onClick={() => setTab("NAMES")}
              active={tab === "NAMES"}
              icon={<CubeIcon className="h-4 w-4" />}
            >
              Names
            </TabButton>
          </div>
          {tab === "HOME" && (
            <>
              <Details data={registrationData} />
              <div className="text-lg font-bold flex items-center space-x-1.5 text-gray-600">
                <CollectionIcon className="h-5 w-5" />
                <div>Events</div>
              </div>
              <Events data={registrationData} />
            </>
          )}
          {tab === "RECORDS" && <Records data={registrationData} />}
          {tab === "WALLETS" && <Wallets data={textRecords} />}
          {tab === "NAMES" && <Names data={registrationData} />}
        </GridItemEight>
      </GridLayout>
      <Support />
    </>
  );
}

export async function getServerSideProps(context) {
  const ensName = context.query.ens;
  const hash = namehash.hash(context.query.ens);

  const hashData = await client
    .query(
      gql`
        query ($id: String) {
          domain(id: $id) {
            labelhash
          }
        }
      `,
      { id: hash }
    )
    .toPromise();

  context.res.setHeader("Cache-Control", "public, s-maxage=86400");

  if (hashData.data.domain) {
    const graphData = await client
      .query(ensQuery, { id: hashData.data.domain.labelhash })
      .toPromise();

    if (graphData.data.registration) {
      const res = await fetch(`https://eth.xyz/text-records/${ensName}`);
      const textRecords = await res.json();

      return {
        props: {
          registrationData: graphData.data.registration,
          textRecords: textRecords,
        },
      };
    } else {
      return { notFound: true };
    }
  } else {
    return { notFound: true };
  }
}
