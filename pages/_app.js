import "../styles/styles.css";
import { Toaster } from "react-hot-toast";

function ENSDebugger({ Component, pageProps }) {
  return (
    <>
      <Toaster position="top-right" />
      <Component {...pageProps} />
    </>
  );
}

export default ENSDebugger;
