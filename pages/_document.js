import Document, { Head, Html, Main, NextScript } from "next/document";

class ENSDebuggerDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link rel="icon" type="image/png" href="/favicon.png" />
          <link
            href="https://assets.devparty.io/css/font.css"
            rel="stylesheet"
          />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default ENSDebuggerDocument;
