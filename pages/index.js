/* eslint-disable @next/next/no-img-element */
import Head from "next/head";
import { useState } from "react";
import { createClient, gql } from "urql";
import Support from "../components/Support";
import { API_URL } from "../components/utils/constants";

const searchQuery = gql`
  query ($name: String) {
    domains(
      first: 5
      where: {
        name_contains: $name
        name_ends_with: ".eth"
        resolvedAddress_not: null
      }
    ) {
      name
    }
  }
`;

const client = createClient({ url: API_URL });

export default function Home() {
  const [searchText, setSearchText] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleSearch = async (evt) => {
    let keyword = evt.target.value;
    setSearchText(keyword);
    setLoading(true);
    let searchData = await client
      .query(searchQuery, { name: keyword })
      .toPromise();
    setSearchResults(searchData?.data?.domains);
    setLoading(false);
  };

  const title = "ENS.events";
  const description = "Check the events of your ENS name and when it happened";

  return (
    <div className="home-bg">
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={description} />
        <meta property="og:image" content="https://ens.events/default.jpeg" />
        <meta property="twitter:title" content={title} />
        <meta property="twitter:description" content={description} />
        <meta
          property="twitter:image"
          content="https://ens.events/default.jpeg"
        />
      </Head>
      <div className="flex flex-col items-center justify-center px-5 h-screen">
        <div className="w-full lg:w-1/4 md:w-2/4 sm:w-3/4">
          <div className="flex items-center bg-white border border-gray-200 rounded-full p-4 shadow text-xl">
            <img
              className="w-6 h-6 ml-auto mr-auto rounded"
              src="/logo.svg"
              alt="ENS Logo"
            />
            <input
              type="text"
              className="w-full outline-none px-3"
              placeholder="Search any ENS name..."
              value={searchText}
              onChange={handleSearch}
            />
            {loading && <div>⌛</div>}
          </div>
          <div className="mt-5">
            {searchText.length > 0 && searchResults.length > 0 && (
              <div className="bg-white border shadow-lg rounded-xl divide-y">
                {searchResults?.map((domain, index) => (
                  <div key={index} className="py-2 px-3">
                    <a href={`/${domain.name}`}>
                      <div className="truncate text-lg">{domain.name}</div>
                    </a>
                  </div>
                ))}
              </div>
            )}
          </div>
        </div>
      </div>
      <Support />
    </div>
  );
}
